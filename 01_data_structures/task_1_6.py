# -*- coding: utf-8 -*-
'''
Задание 1.6

Обработать строку vova и вывести информацию на стандартный поток вывода в виде:
name:                  Владимир
ip:                    10.0.13.3
город:                 Moscow
date:                  15.03.2020
time:                  15:20

Ограничение: Все задания надо выполнять используя только пройденные темы.

'''

vova = 'O Владимир       15.03.2020 15:20 10.0.13.3, 3d18h, Moscow/5'
items = vova.split()
items = [i.strip(',') for i in items]

print("name:\t\t\t",items[1])
print("ip:\t\t\t",items[4])
city = items[6]
print("город:\t\t\t",city[0:city.find('/')])
print("date:\t\t\t", items[2])
print("time:\t\t\t", items[3])