# -*- coding: utf-8 -*-
'''
Задание 1.8

Дано:
student - кол-во школьников
apple - кол-во яблок

Школьники делят между собой  поровну яблоки из корзины
Оставшиеся яблоки остаются в корзине
Подсчитать сколько яблок достанется одному школьнику

Вариант для продвинутых:
Дополнительно написать код, что число школьников 
и яблок - задается с клавиатуры пользователем
'''


student = 7
apple = 59

share = apple // student
print(share)

student = int(input("Enter number of students: "))
apple = int(input("Enter number of apples: "))

share = apple // student
print(share)