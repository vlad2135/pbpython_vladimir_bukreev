'''
Задание 3.1b
Скопировать код из предыдущего задания


Допишите код, чтобы:
    - после вывода результата код запускался заново и опять запрашивал ввод выражения;
    - при вводе пустой строки программа останавливалась.

'''
import sys

while True:
    expression = input("Введите выражение: ")
    if not expression:
        break

    operations = {
        "+": ("сложения", lambda x, y: x + y),
        "-": ("вычитания", lambda x, y: x - y),
        "/": ("деления", lambda x, y: x / y),
        "*": ("умножения", lambda x, y: x * y),
    }

    isOperatorFound = False

    for operator in ['+', '-', '/', '*']:
        if operator not in expression:
            continue

        isOperatorFound = True

        operands = expression.split(operator)
        ints = [int(e) for e in operands]
        name = operations[operator][0]
        operation = operations[operator][1]

        print(f"Результат {name}:", operation(ints[0], ints[1]))
        break

    if not isOperatorFound:
        print("Невалидное выражение", file=sys.stderr)
