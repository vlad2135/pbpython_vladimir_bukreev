"""
Здание 4.4

Написать 4 функции
plus_args(v1, v2)   # для сложения aргументов v1 + v2
minus_args(v1, v2)  # для вычетания aргументов v1 - v2
multi_args(v1, v2)  # для умножения аргументов v1 * v2
div_args(v1, v2)    # для деления аргументов v1 / v2

пример запуска функции plus_args :

print(plus_args(5, 9))
Операция - сложение: 5 + 9
14
"""


def plus_args(v1, v2):
    print(f"Операция - сложение: {v1} + {v2}")
    return v1 + v2


def minus_args(v1, v2):
    print(f"Операция - вычитание: {v1} - {v2}")
    return v1 - v2


def multi_args(v1, v2):
    print(f"Операция - умножение: {v1} * {v2}")
    return v1 * v2


def div_args(v1, v2):
    print(f"Операция - деление: {v1} / {v2}")
    return v1 / v2