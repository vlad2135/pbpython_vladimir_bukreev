# -*- coding: utf-8 -*-
'''
Задание 4.1

Обработать строки из файла servers.txt
и вывести информацию по каждому серверу в таком виде:

server:            qa_test1
ip:                10.0.13.3
switch_port:       FastEthernet0/0
uptime:            3d20h

Ограничение: Все задания надо выполнять используя только пройденные темы.
'''
tpl = """server:\t\t\t{0}
ip:\t\t\t{1}
switch_port:\t\t{2}
uptime:\t\t\t{3}
"""

f = open("04_files_and_func/servers.txt", mode="r")
for line in f:
    (o, hostname, squares, via, ip, uptime, ifaceName) = line.split()

    ip = ip.rstrip(',')
    uptime = uptime.rstrip(',')
    print(tpl.format(hostname, ip, ifaceName, uptime))