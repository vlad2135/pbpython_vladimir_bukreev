"""
Здание 4.4a
Cкопируйте функции написаные в предыдущем задании
Написать функции:

validate_operator(operator)
    функция проверяет, что operator равен допустимым значениям
        допустимые значения: + - * /
    Если оператор принимает допустимое значение:
        возвращается True
    Если оператор НЕ принимает допустимое значение:
        выводится строка "Неизвестный оператор"
        И возвращается False

validate_value(val, operator)
    Функция проверяет что значение val либо int либо float
    Если все верно - возвращается True
    Если оператор равен "/" а второй операнд равен нулю:
        Выводится строка: "На ноль делить нельзя!"
        И возвращается - False
    В остальных случаях:
        Выводится строка: "Неподдерживаемый тип опреранда"
        И возвращается - False
    
calculator(v1 :int or float , operator :str, v2 :int or float)

При запуске функции:
    Проверяются оператор и операнды функциями:
        validate_operator
        validate_value
В зависимости от оператора выплнить соответствующую функцию из предыдучего задания
    Результат выполнения записать в переменную result и вернуть в конце
"""


def plus_args(v1, v2):
    print(f"Операция - сложение: {v1} + {v2}")
    return v1 + v2


def minus_args(v1, v2):
    print(f"Операция - вычитание: {v1} - {v2}")
    return v1 - v2


def multi_args(v1, v2):
    print(f"Операция - умножение: {v1} * {v2}")
    return v1 * v2


def div_args(v1, v2):
    print(f"Операция - деление: {v1} / {v2}")
    return v1 / v2


operations = {
    "+": plus_args,
    "-": minus_args,
    "/": div_args,
    "*": multi_args
}


def validate_operator(operator):
    if operator in '+-*/':
        return True
    else:
        print("Неизвестный оператор")
        return False


def validate_value(val):
    if not isinstance(val, int) and not isinstance(val, float):
        print("Неподдерживаемый тип операнда")
        return False

    return True


def calculator(v1: int or float, operator: str, v2: int or float):
    if not validate_operator(operator) or not validate_value(v1) or not validate_value(v2):
        return

    if operator == "/" and v2 == 0:
        print("На ноль делить нельзя!")
        return False

    output = operations[operator](v1, v2)
    return output


print(calculator(3, "*", 2))
