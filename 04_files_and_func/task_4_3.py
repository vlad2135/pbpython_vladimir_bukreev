"""
Задание 4.3
Напишите функцию, которая вычисляет количество дней в месяце.
    На вход подается номер месяца и параметр, который указывает, является ли год високосным.

"""
def daysInMonth(monthNumber, isLeapYear):
    days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    cnt = days[monthNumber - 1]
    if monthNumber == 2 and isLeapYear:
        cnt += 1
    
    return cnt

